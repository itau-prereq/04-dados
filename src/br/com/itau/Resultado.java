package br.com.itau;

import java.util.ArrayList;
import java.util.List;

public class Resultado {
    int quantidade;
    Dado dado;
    List<Integer> listaNumero;

    public Resultado(int quantidade, Dado dado) {
        this.quantidade = quantidade;
        this.dado = dado;
        this.listaNumero = new ArrayList<Integer>();
    }

    public void sortear(){
        List<Integer> lancamentos = new ArrayList();

        for (int i = 1; i <= this.quantidade; i++){
            lancamentos.add(dado.jogarDado());
        }
        this.listaNumero = lancamentos;
    }

    public int soma() {
        int soma = 0;

        for (Integer lancamento : this.listaNumero) {
            soma += lancamento;
        }
        return soma;
    }

}
