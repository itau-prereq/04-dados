package br.com.itau;

import java.util.*;

public class Sorteio {
    public static  List<Resultado> multSorteio(int rodadas, int nDados, Dado dado) {
        List<Resultado> listaRes = new ArrayList<>();

        for (int i = 0; i < rodadas; i++) {
            Resultado resultado = new Resultado(nDados, dado);

            resultado.sortear();
            listaRes.add(resultado);
        }

        return listaRes;
    }
}
