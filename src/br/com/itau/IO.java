package br.com.itau;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

    public class IO {
        public static void imprimeMsgInicial() {
            System.out.println("Bem vindo");
        }

        //DTO - data transfer object
        public static int obterDado() {
            Scanner scanner = new Scanner(System.in);

            System.out.println("Quantidade de lados no dado: ");
            int lados = scanner.nextInt();

            return lados;
        }

        public static int obterDadosPorRodada() {
            Scanner scanner = new Scanner(System.in);

            System.out.println("Numeros de dados a jogar: ");
            int numero = scanner.nextInt();

            return numero;
        }

        public static int obterRodadas() {
            Scanner scanner = new Scanner(System.in);

            System.out.println("Numeros de rodadas: ");
            int numero = scanner.nextInt();

            return numero;
        }

        public static void imprimeResultado(Resultado resultado) {
            StringBuilder sb = new StringBuilder();

            for (Integer lancamento : resultado.listaNumero) {
                sb.append(lancamento.toString() + ",");
            }
            System.out.println(sb);
            System.out.println("Soma: " + resultado.soma());
        }

        public static void imprimeResultado(List<Resultado> sorteios) {
            for (Resultado sorteio : sorteios) {
                imprimeResultado(sorteio);
            }
        }
    }
