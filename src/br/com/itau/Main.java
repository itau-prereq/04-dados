package br.com.itau;

public class Main {

    public static void main(String[] args){
        IO io = new IO();

        io.imprimeMsgInicial();

        Dado dado = new Dado(io.obterDado());
        int nDados = io.obterDadosPorRodada();
        int rodadas = io.obterRodadas();

        io.imprimeResultado(Sorteio.multSorteio(rodadas, nDados, dado));
    }
}
