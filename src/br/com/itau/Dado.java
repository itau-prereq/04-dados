package br.com.itau;

import java.util.Random;

public class Dado {
    int lados;

    public Dado(int lados) {
        this.lados = lados;
    }

    public int jogarDado() {
        Random r = new Random();
        return (r.nextInt(this.lados) + 1);
    }
}


